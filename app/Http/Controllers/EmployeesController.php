<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Employee;
use DB;
class EmployeesController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $employees = Employee::all();
        $employee_counter = [];
        $employee_counter['total'] = Employee::count();
        $employee_counter['per_position'] = DB::table('employees')
                                            ->select(DB::raw("count(*) as count_position, position"))
                                            ->whereNull(DB::raw('deleted_at'))
                                            ->groupBy(DB::raw('position'))
                                            ->get();
        $colors = [];
        $labels = [];
        $values = [];
        foreach ($employee_counter['per_position'] as $key => $value) {
            $colors[] = $this->getRgbaForChartJs($value->count_position * rand());
            $labels[] = $value->position;
            $values[] = $value->count_position;
        }
        $colors = json_encode($colors);
        $labels = json_encode($labels);
        $values = json_encode($values);
        return view('Employees.index', compact('employees', 'employee_counter','colors','labels', 'values'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
        return view('Employees.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        $request->validate([
            'name'=>'required',
            'birthdate'=> 'required',
            'salary' => 'required|integer',
            'position' => 'required',
        ]);
        $emp = new Employee([
            'name' => $request->get('name'),
            'salary'=> $request->get('salary'),
            'position'=> $request->get('position'),
            'birthdate' => $request->get('birthdate'),
            'gender' => $request->get('gender'),
            'skills' => json_encode($request->get('skills'))
        ]);
        $emp->save();
        return redirect('/employees')->with('success', 'Employee has been added');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $employee = Employee::find($id);

        return view('employees.edit', compact('employee'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
        $request->validate([
            'name'=>'required',
            'birthdate'=> 'required',
            'salary' => 'required|integer',
            'position' => 'required',
        ]);
        $emp = Employee::find($id);
        $emp->name = $request->get('name');
        $emp->position = $request->get('position');
        $emp->salary = $request->get('salary');
        $emp->birthdate = $request->get('birthdate');
        $emp->gender = $request->get('gender');
        $emp->skills = json_encode($request->get('skills'));
        $emp->save();
        return redirect('/employees')->with('info', 'Employee has been updated');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $emp = Employee::find($id);
        $emp->delete();

        return redirect('/employees')->with('danger', 'Employee has been deleted');
    }


    /* Private Function */
    private function getRgbaForChartJs($num) {
        $hash = md5('color' . $num); // modify 'color' to get a different palette
        return 'rgba('.hexdec(substr($hash, 0, 2)).', '.hexdec(substr($hash, 2, 2)).', '.hexdec(substr($hash, 4, 2)).', 0.2)';
    }
    /* End Private Function */
}
