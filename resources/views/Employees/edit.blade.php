@extends('layouts.template')

@section('content')
    <div class="card">
        <div class="card-header">
            Edit Employee
        </div>
        <div class="card-body">
            @if($errors->any())
                <div class="alert alert-danger">
                    <ul>
                        @foreach($errors->all() as $error)
                            <li>{{ $error }}</li>
                        @endforeach
                    </ul>
                </div>
            @endif
            <form method="post" action="{{ route('employees.update', $employee->id) }}">
                @method('PATCH')
                @csrf
                <div class="form-group">
                    <label for="name">Name:</label>
                    <input type="text" class="form-control" name="name" value="{{$employee->name}}"/>
                </div>
                <div class="form-group">
                    <label for="name">Position:</label>
                    <input type="text" class="form-control" name="position" value="{{$employee->position}}"/>
                </div>
                <div class="form-group">
                    <label for="name">Gender:</label>
                    <div class="form-check form-check-inline">
                        <input class="form-check-input" type="radio" name="gender" id="gendermale" value="male" {{$employee->gender == 'male' ? 'checked' : ''}}>
                        <label class="form-check-label" for="gendermale">Male</label>
                    </div>
                    <div class="form-check form-check-inline">
                        <input class="form-check-input" type="radio" name="gender" id="genderfemale" value="female" {{$employee->gender == 'female' ? 'checked' : ''}}>
                        <label class="form-check-label" for="genderfemale">Female</label>
                    </div>
                </div>
                <div class="form-group">
                    <label for="name">Skills:</label>
                    <?php $skills = json_decode($employee->skills,TRUE);
                        if(!is_array($skills)) $skills = [];
                    ?>
                    <div class="form-check form-check-inline">
                        <input class="form-check-input" type="checkbox" id="Laravel" value="laravel" name="skills[]" {{in_array('laravel',$skills) ? 'checked' : ''}}>
                        <label class="form-check-label" for="Laravel">Laravel</label>
                    </div>
                    <div class="form-check form-check-inline">
                        <input class="form-check-input" type="checkbox" id="CakePHP" value="cakephp" name="skills[]" {{in_array('cakephp',$skills) ? 'checked' : ''}}>
                        <label class="form-check-label" for="CakePHP">CakePHP</label>
                    </div>
                    <div class="form-check form-check-inline">
                        <input class="form-check-input" type="checkbox" id="ReactNative" value="reactnative" name="skills[]" {{in_array('reactnative',$skills) ? 'checked' : ''}}>
                        <label class="form-check-label" for="ReactNative">React Native</label>
                    </div>
                </div>
                <div class="form-group">
                    <label for="name">Salary:</label>
                    <input type="numeric" class="form-control" name="salary" value="{{$employee->salary}}"/>
                </div>
                <div class="form-group">
                    <label for="name">Birthdate:</label>
                    <input type="date" class="form-control" name="birthdate" value="{{date('Y-m-d',strtotime($employee->birthdate))}}"/>
                </div>
                <button type="submit" class="btn btn-primary">Update</button>
                <a class="btn btn-dark" href="{{url('employees')}}">Back</a>
            </form>
        </div>

    </div>

@stop
