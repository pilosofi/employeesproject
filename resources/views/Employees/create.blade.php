@extends('layouts.template')

@section('content')
    <div class="card">
        <div class="card-header">
            Add Employee
        </div>
        <div class="card-body">
            @if($errors->any())
                <div class="alert alert-danger">
                    <ul>
                        @foreach($errors->all() as $error)
                            <li>{{ $error }}</li>
                        @endforeach
                    </ul>
                </div>
            @endif
            <form method="post" action="{{ route('employees.store') }}">
                <div class="form-group">
                    @csrf
                    <label for="name">Name:</label>
                    <input type="text" class="form-control" name="name"/>
                </div>
                <div class="form-group">
                    <label for="name">Position:</label>
                    <input type="text" class="form-control" name="position"/>
                </div>
                <div class="form-group">
                    <label for="name">Gender:</label>
                    <div class="form-check form-check-inline">
                        <input class="form-check-input" type="radio" name="gender" id="gendermale" value="male" checked>
                        <label class="form-check-label" for="gendermale">Male</label>
                    </div>
                    <div class="form-check form-check-inline">
                        <input class="form-check-input" type="radio" name="gender" id="genderfemale" value="female">
                        <label class="form-check-label" for="genderfemale">Female</label>
                    </div>
                </div>
                <div class="form-group">
                    <label for="name">Skills:</label>
                    <div class="form-check form-check-inline">
                        <input class="form-check-input" type="checkbox" id="Laravel" value="laravel" name="skills[]">
                        <label class="form-check-label" for="Laravel">Laravel</label>
                    </div>
                    <div class="form-check form-check-inline">
                        <input class="form-check-input" type="checkbox" id="CakePHP" value="cakephp" name="skills[]">
                        <label class="form-check-label" for="CakePHP">CakePHP</label>
                    </div>
                    <div class="form-check form-check-inline">
                        <input class="form-check-input" type="checkbox" id="ReactNative" value="reactnative" name="skills[]">
                        <label class="form-check-label" for="ReactNative">React Native</label>
                    </div>
                </div>
                <div class="form-group">
                    <label for="name">Salary:</label>
                    <input type="numeric" class="form-control" name="salary"/>
                </div>
                <div class="form-group">
                    <label for="name">Birthdate:</label>
                    <input type="date" class="form-control" name="birthdate" value=""/>
                </div>
                <button type="submit" class="btn btn-primary">Add</button>
                <a class="btn btn-dark" href="{{url('employees')}}">Back</a>
            </form>
        </div>

    </div>

@stop
